# Unlimited Custom Markers mod for The Witcher 3

## UPDATE
This is a fork from https://gitlab.com/pcworld/witcher3-unlimited-custom-markers, in order to patch for v4.04 which slightly broke memory addresses for the hot-patching.

## Original README.md

This mod removes The Witcher 3's limitation of the number of custom markers that can be placed on the map (which is by default 10).

It should be compatible with all The Witcher 3 builds from v1.24 to v1.31 as well as v4.0.0 (and possibly later versions).
Please report if it does not work on some build or future versions of the game.

Note that it is unknown why the markers limit is in the game in the first place.
It cannot be excluded that removing the limit could lead to save game corruption or other issues, though none have been reported as of yet.
Use at your own risk.

The source code is hosted at https://gitlab.com/shadyfennec/witcher3-unlimited-custom-markers

## Installation

[Download a release](https://gitlab.com/shadyfennec/witcher3-unlimited-custom-markers/-/releases).
Extract the release's zip file into your Witcher 3 installation directory (e.g., `C:\Program Files (x86)\GOG Galaxy\Games\The Witcher 3 Wild Hunt GOTY` or `C:\Program Files (x86)\Steam\steamapps\common\The Witcher 3`).
If it prompts that it would replace `dsound.dll`, chances are that you already have the correct `dsound.dll` from another mod (such as [this Witcher 3 weight mod](https://github.com/fatalis/witcher3weight)).
The `plugins` folder should be merged then.

### Technical Notes

The `dsound.dll` delivered with this mod is taken from [fatalis's Witcher 3 weight mod](https://github.com/fatalis/witcher3weight) and originally stems from [Karl Skomski's dll-plugins-loader](https://github.com/gamebooster/dll-plugins-loader).
It loads all `.dll`s from a directory called `plugins`, thus can support loading multiple mods that need native code.

From Witcher 3 v4.0.0 on ("Complete Edition"), the game has two binary folders, `bin\x64` for the old DirectX11 renderer and `bin\x64_dx12` for the new DirectX12 renderer.
The mod works the same for both, but the mod files need to be placed in both directories accordingly.
The mod release already accounts for this by including the mod files twice for both directories.

## Notes for developers

### Building

This mod has been developed with GCC from [mingw-w64](https://www.mingw-w64.org/downloads/).
Compiling with Microsoft's own compilers may or may not work (feel free to create an issue if you find out).

```
x86_64-w64-mingw32-g++ -Wall -Werror -shared -std=c++17 -O3 -o unlimited-custom-markers.dll unlimited-custom-markers.cpp
```

### Implementation details

The game sets a limit of 10 markers in the WitcherScript `content\content0\scripts\game\gui\menus\mapMenu.ws`.
However, patching the script only allows to increase the limit to 20, because the latter limit is enforced in the game executable.
This is why patching native code appears to be necessary.

## Credits

Some code for hooking The Witcher 3 functions has been ported to C++ from [fatalis's Witcher 3 weight mod](https://github.com/fatalis/witcher3weight) (which is written in D).

## History

The original version of this mod from 2017 had only been published [on the NexusMods forums](https://forums.nexusmods.com/index.php?/topic/4575465-mod-requesthelp-more-custom-map-markers/?p=54588953).
