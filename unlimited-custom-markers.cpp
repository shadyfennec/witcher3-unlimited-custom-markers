#include <windows.h>
#include <Psapi.h>
#include <string>
#include <tuple>
#include <vector>

void* GetScriptFunc(const char16_t* name);
void* FindBytes(const void* search, size_t size, void* mem, size_t len);
uint8_t* FindBytesReverse(const void* search, size_t size, const void* mem, size_t len);
void* FindLEARDXForAddr(const void* addr, const void* mem, size_t size);

extern "C" BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	if (fdwReason != DLL_PROCESS_ATTACH)
		return true;

	std::vector<std::tuple<std::vector<unsigned char>, std::vector<unsigned char>, unsigned char>> replacement_alternatives;
	// Contains (old_bytes, new_bytes, offset from GetUserMapPinLimits) tuples.
	// The first entry that matches will be replaced and the others ignored.

	// Approach of this mod:
	// Determine the address of GetUserMapPinLimits, and always set the max custom markers count to 0xFFFFFFFF.
	// Makes it more dynamic as this code is relatively easy to be found and doesn't need patching of Witcher 3 scripts.
	uint8_t* GetUserMapPinLimits = (uint8_t*) GetScriptFunc(u"GetUserMapPinLimits");

	replacement_alternatives = {
		{ // Witcher 3 v1.24-v1.31
			// old_bytes (addresses from Galaxy 1.31 GOTY):
			// 0000000140C41D06                      | 83 38 14                      | cmp dword ptr ds:[rax],14                      | determines if otherPinLimit < 20
			// 0000000140C41D09                      | 72 0B                         | jb witcher3.140C41D16                          |
			// 0000000140C41D0B                      | 48 8B 44 24 30                | mov rax,qword ptr ss:[rsp+30]                  |
			// 0000000140C41D10                      | C7 00 14 00 00 00             | mov dword ptr ds:[rax],14                      |
			{0x83, 0x38, 0x14, 0x72, 0x0B, 0x48, 0x8B, 0x44, 0x24, 0x30, 0xC7, 0x00, 0x14, 0x00, 0x00, 0x00},
			// new_bytes (addresses from Galaxy 1.31 GOTY):
			// 0000000140C41D06                      | 90                            | nop                                            |
			// 0000000140C41D07                      | 90                            | nop                                            |
			// 0000000140C41D08                      | 90                            | nop                                            |
			// 0000000140C41D09                      | 90                            | nop                                            |
			// 0000000140C41D0A                      | 90                            | nop                                            |
			// 0000000140C41D0B                      | 90                            | nop                                            |
			// 0000000140C41D0C                      | 90                            | nop                                            |
			// 0000000140C41D0D                      | 90                            | nop                                            |
			// 0000000140C41D0E                      | 90                            | nop                                            |
			// 0000000140C41D0F                      | 90                            | nop                                            |
			// 0000000140C41D10                      | C7 00 FF FF FF FF             | mov dword ptr ds:[rax],FFFFFFFF                |
			{0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0xC7, 0x00, 0xFF, 0xFF, 0xFF, 0xFF},
			0xF6 // offset from GetUserMapPinLimits
		},
		{ // v4.00_Hotfix2 (Complete Edition), works for both x64 and x64_dx12 builds:
			// old_bytes (addresses from v4.00_Hotfix2 GOTY edition on GOG):
			// 00007FF74960E10F | 8339 14                  | cmp dword ptr ds:[rcx],14               |
			// 00007FF74960E112 | 72 08                    | jb witcher3.7FF74960E11C                |
			// 00007FF74960E114 | C701 14000000            | mov dword ptr ds:[rcx],14               |
			{0x83, 0x39, 0x14, 0x72, 0x08, 0xC7, 0x01, 0x14, 0x00, 0x00, 0x00},
			// new_bytes:
			// 00007FF74960E10F | 90                       | nop                                     |
			// 00007FF74960E110 | 90                       | nop                                     |
			// 00007FF74960E111 | 90                       | nop                                     |
			// 00007FF74960E112 | 90                       | nop                                     |
			// 00007FF74960E113 | 90                       | nop                                     |
			// 00007FF74960E114 | C701 FFFFFFFF            | mov dword ptr ds:[rcx],FFFFFFFF         |
			{0x90, 0x90, 0x90, 0x90, 0x90, 0xC7, 0x01, 0xFF, 0xFF, 0xFF, 0xFF},
			0x9F // offset
		},
		{
			// shadyfennec patch for version v4.04 from 2024-06-21
			// old_bytes (found from my copy on steam):
			// 00007FF74960E10F | 8339 14                  | cmp dword ptr ds:[rcx],14               |
			// 00007FF74960E112 | 72 06                    | jb witcher3+1F19C17                     |
			// 00007FF74960E114 | C701 14000000            | mov dword ptr ds:[rcx],14               |
			{0x83, 0x39, 0x14, 0x72, 0x06, 0xC7, 0x01, 0x14, 0x00, 0x00, 0x00},
			// new_bytes:
			// 00007FF74960E10F | 90                       | nop                                     |
			// 00007FF74960E110 | 90                       | nop                                     |
			// 00007FF74960E111 | 90                       | nop                                     |
			// 00007FF74960E112 | 90                       | nop                                     |
			// 00007FF74960E113 | 90                       | nop                                     |
			// 00007FF74960E114 | C701 FFFFFFFF            | mov dword ptr ds:[rcx],FFFFFFFF         |
			{0x90, 0x90, 0x90, 0x90, 0x90, 0xC7, 0x01, 0xFF, 0xFF, 0xFF, 0xFF},
			0x9C // offset
		}
	};

	if (GetUserMapPinLimits == nullptr) {
		MessageBox(nullptr, "unlimited-custom-markers mod: Could not find GetUserMapPinLimits, mod will not work. Report this on nexusmods.", nullptr, MB_ICONERROR);
		return true;
	}

	for (const auto& [old_bytes, new_bytes, offset] : replacement_alternatives) {
		if (old_bytes.size() != new_bytes.size()) {
			MessageBox(nullptr, "unlimited-custom-markers mod: Programming mistake, mod will not work. Report this on nexusmods.", nullptr, MB_ICONERROR);
			return true;
		}
		// Note: To be completely correct, one would need to ensure we are not reading/writing outside of process memory
		if (memcmp(GetUserMapPinLimits + offset, old_bytes.data(), old_bytes.size()) == 0) {
			DWORD lpflOldProtect;
			VirtualProtect(GetUserMapPinLimits + offset, new_bytes.size(), PAGE_EXECUTE_READWRITE, &lpflOldProtect);
			memcpy(GetUserMapPinLimits + offset, new_bytes.data(), new_bytes.size());

			// reset page permissions
			VirtualProtect(GetUserMapPinLimits + offset, new_bytes.size(), lpflOldProtect, &lpflOldProtect);

			MessageBox(nullptr, "unlimited-custom-markers mod: done patching", nullptr, MB_ICONERROR);
			return true;
		}
	}

	MessageBox(nullptr, "unlimited-custom-markers mod: Memory not as expected, mod will not work. Are you running a compatible game version? Possibly report this on nexusmods.", nullptr, MB_ICONERROR);
	return true;
}


// based on https://github.com/fatalis/witcher3weight/blob/master/dllmain.d (which was released under the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE v2):

void* GetScriptFunc(const char16_t* name) {
    HMODULE mod = GetModuleHandleA(nullptr);

    MODULEINFO info;
    if (!GetModuleInformation(GetCurrentProcess(), mod, &info, sizeof(info)))
        return nullptr;

    void* base = info.lpBaseOfDll;
    DWORD size = info.SizeOfImage;

    void* nameAddr = FindBytes(&name[0], std::char_traits<char16_t>::length(name) * sizeof(char16_t), base, size);
    if (!nameAddr)
        return nullptr;

    void *leaAddr = FindLEARDXForAddr(nameAddr, base, size);
    if (!leaAddr)
        return nullptr;

    // witcher.exe (v1.31):    140d8fca2:   48 8d 05 a7 1d ff ff    lea    -0xe259(%rip),%rax        # 0x140d81a50
    unsigned char lea2[] = {0x48, 0x8D, 0x05};
    uint8_t* lea2Addr = FindBytesReverse(&lea2[0], sizeof(lea2), leaAddr, 128 /* kind of arbitrary */);
    if (!lea2Addr)
        return nullptr;

    int32_t offset = *((int32_t*) (lea2Addr+3));
    void* funcAddr = lea2Addr + 7 /* our LEA instruction is 7 bytes long */ + offset;

    return funcAddr;
}

void* FindBytes(const void* search, size_t size, void* mem, size_t len) {
    for (uint8_t* ptr = (uint8_t*) mem; ptr < ((uint8_t*) mem) + len - size; ptr++) {
        if (memcmp(ptr, search, size) == 0) {
            return ptr;
        }
    }

    return nullptr;
}

uint8_t* FindBytesReverse(const void* search, size_t size, const void* mem, size_t len) {
    for (uint8_t* ptr = ((uint8_t*) mem) - size; ptr >= ((uint8_t*) mem) - len; ptr--) {
        if (memcmp(ptr, search, size) == 0) {
            return ptr;
        }
    }

    return nullptr;
}

// .text:00007FF6B424ED9A 48 8D 15 A7 B3 71 01    lea rdx, aGetitemweight
// witcher3.exe (v1.31):    140d8fcc2:   48 8d 15 37 4b 48 01    lea    0x1484b37(%rip),%rdx        # 0x142214800
void* FindLEARDXForAddr(const void* addr, const void* mem, size_t size) {
    unsigned char lea[] = {0x48, 0x8D, 0x15};

	for (uint8_t* ptr = (uint8_t*) mem; ptr < ((uint8_t*) mem) + size - 7 /* our LEA instruction is 7 bytes */; ptr++) {
		int32_t encodedAddr = ((uint8_t*) addr) - (ptr + 7); // "+7": RIP is for the next (not current) instruction, and our LEA instruction is 7 bytes long
		if (memcmp(ptr, &lea[0], sizeof(lea)) == 0 && *((int32_t*) (ptr + 3)) == encodedAddr) {
			return ptr;
		}
	}

    return nullptr;
}
